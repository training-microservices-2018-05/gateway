package com.artivisi.training.microservice201805.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}
}

@RestController
class ConfigServerFallback {
	@GetMapping("/configserveroffline")
	public Map<String, String> configServerOffline() {
		Map<String, String> hasil = new HashMap<>();
		hasil.put("success", "false");
		hasil.put("message", "configserver offline");
		return hasil;
	}
}